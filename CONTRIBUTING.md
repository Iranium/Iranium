# Contribution Guide
Thank you for your interest in making Iranium a better plug-in. Of course, without the help of contributers, no open source project can survive.

Before contributing, please pay attention to following notes:

### Creating issues
* Please **explain** the situation, and if possible, write how to reproduce it.
* For the sake of publicity and open source nature, use English language.
* Be sure to include PHP, WordPress and Apache/nginx versions you are using.
* Issue Tracking is place to track issues and provide voluntary support.

### Contributing to code
* Be sure to run tests before commits.
* Follow the coding standards of WordPress.
* Write tests, if your changes are major.
* Be sure to remove all debugging bridges and Logger calls.