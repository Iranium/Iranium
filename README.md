# Iranium :calendar: :ir:
> Persian calendar system for WordPress

![Build Status](https://gitlab.com/Iranium/Iranium/badges/master/build.svg)

Iranium is a plug-in for WordPress, which enables Persian calendar (also known as Solar Hijri or Djalali). Iranium is created and maintained with **maximum performance and compability** in mind. Iranium follows the modern standards of development.

## Key Features
* Well-tested converting functions
* Standard coding style
* Light-weight hooks
* Highly compatible with other plug-ins
* Supporting Afghan month names

## Why another plug-in?
There are at least three similar plug-ins in the market other than this. Iranium focuses on "performance & integration", instead of "marketing, commercial and rivalry purposes". Iranium does not show you advertisments. Iranium does not force you to ask your questions in external forums. Iranium tries to keep up with the latest updates. Iranium does not offer you unnecessary components.

### What do you mean by "unnecessary components"?
Similar plug-ins have a wide range of unnecessary components. However they don't affect your website performance directly, they have the potential of being used as exploits, even if nothing else, they weigh down your website codes, mess up with your pretty WordPress.

### Why should I use Iranium instead of X?
First of all, Iranium is NOT a commercial project. We are not offering premuium support or any profitable services. Instead, we focus on our solemn task: "improve your experience". So, the following reasons don't mean that we underestimate other developers works.

* Iranium is light-weight. That means we won't slow your website down or add extra tasks unless they are necessary.
* Iranium provides a "dynamic plug-in compability" feature. You can tell Iranium to stop messing up with their datestamps, via a human-readable interface.
* You name it.

## Requirements
* WordPress 4.8 or later
* PHP 5.6 or later (PHP 7 or later is strongly advised)

## Installation
Simply, download [tarball file](https://gitlab.com/Iranium/Iranium/repository/master/archive.zip) and extract it into `wp-content/plugins`. Then, activate the plug-in in WordPress administration.

The plug-in will be published in WordPress official repository soon.