<?php
/**
 * Iridium.
 * Iridium provides a class and required functions for localizing dates,
 * replacing Arabic characters with proper Persian characters, replacing
 * English digits with proper Persian digits.
 * 
 * @package Iranium
 * @subpackage Iridium
 * @author ehsaan
 */

if ( ! defined( 'ABSPATH' ) ) exit; // No direct access allowed.

/**
 * Iridium Class.
 * Providing required functions for Iranium to work properly.
 * 
 * @since 1.0
 */
final class Iridium {
    /**
     * @var array Month names.
     */
    private $month_names = [];

    /**
     * @var array Month names, short version.
     */
    private $short_month_names = [];

    /**
     * @var array Seasons names.
     */
    private $seasons_names = [];

    /**
     * @var array Week days names.
     */
    private $week_days_names = [];

    /**
     * @var array Week days names, short version.
     */
    private $short_week_days_names = [];

    /**
     * @var array Count of days in every month.
     */
    private $days_in_month = [ 31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29 ];

    /**
     * @var array Sum of the spent days based on Iranian month number (staring from 1).
     */
    private $p_days_sum_month = [ 0, 0, 31, 62, 93, 124, 155, 186, 216, 246, 276, 306, 336 ];

    /**
     * @var array Sum of the spent days based on Gregorian month number (starting from 1).
     */
    private $g_days_sum_month = [ 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 ];

    /**
     * @var array English digits list.
     */
    private $english_digits = [ '0', '1', '2', '3',
                            '4', '5', '6', '7',
                            '8', '9' ];
    
    /**
     * @var array Arabic digits list.
     */
    private $arabic_digits = [ '٠', '١', '٢', '٣',
                        '٤', '٥', '٦', '٧',
                        '٨', '٩' ];
    
    /**
     * @var array Persian digits list.
     */
    private $persian_digits = [ '۰', '۱', '۲', '۳',
                        '۴', '۵', '۶', '۷',
                        '۸', '۹' ];
    
    /**
     * @var array Arabic characters those are needed to be replaced.
     */
    private $arabic_chars = [ 'ة', 'ك', 'ي' ];

    /**
     * @var array Persian characters replacing Arabic equivalents.
     */
    private $persian_chars = [ 'ه', 'ک', 'ی' ];

    /**
     * Iridum bootstrapper fills the arrays required for working.
     * It also detects that if the current year is leap or not.
     * 
     * @since 1.0
     * @return void
     */
    public function bootstrap() {
        global $iranium_options;

        if ( $iranium_options['afghan_month_names'] == 'enabled' ) {
            /**
             * @todo These names are needed to be verified by an
             * Afghan. For now, these names are taken from byu.edu.
             * 
             * @see http://sartre2.byu.edu/persian/reference/MonthsAfghan.html
             */
            /**
             * Possible bug in WordPress.
             * _x() wasn't working in this case, so I had to use __()
             * instead _x() and had no other way but to insert the context
             * in code as comments.
             */
            $this->month_names = [
                // Afghanian 1st month name.
                __( 'Hamal', 'iranium' ),
                // Afghanian 2nd month name.
                __( 'Sawr', 'iranium' ),
                // Afghanian 3rd month name.
                __( 'Jawzā', 'iranium' ),
                // Afghanian 4th month name.
                __( 'Saratān', 'iranium' ),
                // Afghanian 5th month name.
                __( 'Asad', 'iranium' ),
                // Afghanian 6th month name.
                __( 'Sombole', 'iranium' ),
                // Afghanian 7th month name.
                __( 'Mizān', 'iranium' ),
                // Afghanian 8th month name.
                __( '`Aqrab', 'iranium' ),
                // Afghanian 9th month name.
                __( 'Qaws', 'iranium' ),
                // Afghanian 10th month name.
                __( 'Jady', 'iranium' ),
                // Afghanian 11th month name.
                __( 'Dalv', 'iranium' ),
                // Afghanian 12th month name.
                __( 'Hut', 'iranium' )
            ];
            $this->short_month_names = [
                // Afghanian 1st short month name.
                __( 'Ham', 'iranium' ),
                // Afghanian 2nd short month name.
                __( 'Saw', 'iranium' ),
                // Afghanian 3rd short month name.
                __( 'Jaw', 'iranium' ),
                // Afghanian 4th short month name.
                __( 'Sar', 'iranium' ),
                // Afghanian 5th short month name.
                __( 'Asa', 'iranium' ),
                // Afghanian 6th short month name.
                __( 'Som', 'iranium' ),
                // Afghanian 7th short month name.
                __( 'Miz', 'iranium' ),
                // Afghanian 8th short month name.
                __( 'Aqr', 'iranium' ),
                // Afghanian 9th short month name.
                __( 'Qaw', 'iranium' ),
                // Afghanian 10th short month name.
                __( 'Jad', 'iranium' ),
                // Afghanian 11th short month name.
                __( 'Dal', 'iranium' ),
                // Afghanian 12th short month name.
                __( 'Hut', 'iranium' )
            ];
        } else {
            $this->month_names = [
                // Persian 1st month name
                __( 'Farvardin', 'iranium' ),
                // Persian 2nd month name
                __( 'Ordibehesht', 'iranium' ),
                // Persian 3rd month name
                __( 'Khordad', 'iranium' ),
                // Persian 4th month name
                __( 'Tir', 'iranium' ),
                // Persian 5th month name
                __( 'Mordad', 'iranium' ),
                // Persian 6th month name
                __( 'Shahrivar', 'iranium' ),
                // Persian 7th month name
                __( 'Mehr', 'iranium' ),
                // Persian 8th month name
                __( 'Āban', 'iranium' ),
                // Persian 9th month name
                __( 'Āzar', 'iranium' ),
                // Persian 10th month name
                __( 'Dey', 'iranium' ),
                // Persian 11th month name
                __( 'Bahman', 'iranium' ),
                // Persian 12th month name
                __( 'Esfand', 'iranium' )
            ];
            $this->short_month_names = [
                // Persian 1st short month name
                __( 'Far', 'iranium' ),
                // Persian 2nd short month name
                __( 'Ord', 'iranium' ),
                // Persian 3rd short month name
                __( 'Kho', 'iranium' ),
                // Persian 4th short month name
                __( 'Tir', 'iranium' ),
                // Persian 5th short month name
                __( 'Mor', 'iranium' ),
                // Persian 6th short month name
                __( 'Sha', 'iranium' ),
                // Persian 7th short month name
                __( 'Meh', 'iranium' ),
                // Persian 8th short month name
                __( 'Aba', 'iranium' ),
                // Persian 9th short month name
                __( 'Aza', 'iranium' ),
                // Persian 10th short month name
                __( 'Dey', 'iranium' ),
                // Persian 11th short month name
                __( 'Bah', 'iranium' ),
                // Persian 12th short month name
                __( 'Esf', 'iranium' )
            ];
        }

        $this->seasons_names = [
            // Persian/Dari first season name.
            __( 'Spring', '', 'iranium' ),
            // Persian/Dari second season name.
            __( 'Summer', 'iranium' ),
            // Persian/Dari third season name.
            __( 'Autumn', 'iranium' ),
            // Persian/Dari fourth season name.
            __( 'Winter', 'iranium' )
        ];

        $this->week_days_names = [
            // Persian/Dari first day of week name.
            __( 'Saturday', 'iranium' ),
            // Persian/Dari second day of week name.
            __( 'Sunday', 'iranium' ),
            // Persian/Dari third day of week name.
            __( 'Monday', 'iranium' ),
            // Persian/Dari fourth day of week name.
            __( 'Tuesday', 'iranium' ),
            // Persian/Dari fifth day of week name.
            __( 'Wednesday', 'iranium' ),
            // Persian/Dari sixth day of week name.
            __( 'Thursday', 'iranium' ),
            // Persian/Dari seventh day of week name.
            __( 'Friday', 'iranium' )
        ];
        
        $this->short_week_days_names = [
            // Persian/Dari first day of week name (short).
            __( 'Sat', 'iranium' ),
            // Persian/Dari second day of week name (short).
            __( 'Sun', 'iranium' ),
            // Persian/Dari third day of week name (short).
            __( 'Mon', 'iranium' ),
            // Persian/Dari fourth day of week name (short).
            __( 'Tue', 'iranium' ),
            // Persian/Dari fifth day of week name (short).
            __( 'Wed', 'iranium' ),
            // Persian/Dari sixth day of week name (short).
            __( 'Thu', 'iranium' ),
            // Persian/Dari seventh day of week name (short).
            __( 'Fri', 'iranium' )
        ];

        /**
         * Check if current year is leap or not.
         * If it is leap, add one day to 12th month days.
         */
        if ( $this->is_leap_year( $this->persian_date( 'Y' ) ) )
            $this->days_in_month[ 11 ] = 30;
    }

    /**
     * Returns true if the given year is leap.
     * 
     * @see https://goo.gl/wZCU76
     * @param int $year
     * @return boolean
     */
    public function is_leap_year( $year ) {
        $a = 0.025;
        $b = 266;

        if ( $year <= 0 ) return false;
        
        $c = ( ( $year + 38 ) % 2820 ) * 0.24219 + $a; // 0.24219 ~ Extra days of a year
        $d = ( ( $year + 39 ) % 2820 ) * 0.24219 + $a; // 38 days is the difference of epoch to 2820-year circle.

        $frac_c = intval( ( $c - intval( $c ) ) * 1000 );
        $frac_d = intval( ( $d - intval( $d ) ) * 1000 );

        return ( $frac_c <= $b && $frac_d > $b );
    }

    /**
     * Converts the given Gregorian datestamp to Persian datestamp.
     * Supports most of the `date()` format characters.
     * 
     * @see date
     * @see strtotime
     * @see Iridium::gregorian_to_persian
     * @param string $format Output datestamp format (as in PHP date function)
     * @param string $date Input datestamp (must be known by strtotime)
     * @return string Proper formatted Persian datestamp.
     */
    public function persian_date( $format, $date = 'now' ) {
        $timestamp = is_numeric( $date ) ? $date : strtotime( $date );
        $date = getdate( $timestamp );

        list( $date[ 'year' ], $date[ 'mon' ], $date[ 'mday' ] ) = $this->gregorian_to_persian( $date[ 'year' ], $date[ 'mon' ], $date[ 'mday' ] );
        
        $output = '';

        // Process each character and replace them with proper value.
        for ( $i = 0; $i < strlen( $format ); $i++ ) {
            switch ( $format[ $i ] ) {
                case 'd':
                    $output .= sprintf( '%02d', $date[ 'mday' ] ); // Adds a leading zero.
                    break;
                case 'D':
                    $output .= $this->short_week_days_names[ $date[ 'wday' ] ];
                    break;
                case 'l':
                    $output .= $this->week_days_names[ $date[ 'wday' ] ];
                    break;
                case 'j':
                    $output .= $date[ 'mday' ];
                    break;
                case 'N':
                    $output .= $this->week_day( $date[ 'wday' ] ) + 1;
                    break;
                case 'w':
                    $output .= $this->week_day( $date[ 'wday' ] );
                    break;
                case 'z':
                    $output .= $this->days_in_month[ $date[ 'mon' ] ] + $date[ 'mday' ];
                    break;
                case 'W':
                    $day_of_year = $this->p_days_sum_month[ $date[ 'mon' ] ] + $date[ 'mday' ]; // ??
                    $output .= intval( $day_of_year / 7 );
                    break;
                case 'f':
                    $output .= $this->seasons_names[ floor( $date[ 'mon' ] / 3 ) ];
                    break;
                case 'F':
                    $output .= $this->month_names[ ( int ) $date[ 'mon' ] - 1 ];
                    break;
                case 'm':
                    $output .= sprintf( '%02d', $date[ 'mon' ] ); // Adds a leading zero.
                    break;
                case 'M':
                    $output .= $this->short_month_names[ ( int ) $date[ 'mon' ] - 1 ];
                    break;
                case 'n':
                    $output .= $date[ 'mon' ];
                    break;
                case 't':
                    $output .= $this->days_in_month[ ( int ) $date[ 'mon' ] - 1 ]; // ??
                    break;
                case 'L':
                    $output .= $this->is_leap_year( $date[ 'year' ] ) ? '1' : '0';
                    break;
                case 'o':
                case 'Y':
                    $output .= $date[ 'year' ];
                    break;
                case 'y':
                    $output .= substr( $date[ 'year' ], 2, 2 );
                    break;
                case 'a':
                    /**
                     * @todo I am not sure if AM and PM equivalents in Dari
                     * are same as Persian.
                     */
                    $output .= ( $date[ 'hours' ] < 12 ) ? __( 'AM', 'iranium' ) : __( 'PM', 'iranium' );
                    break;
                case 'A':
                    /**
                     * @todo I am not sure if AM and PM equivalents in Dari
                     * are same as Persian.
                     */
                    $output .= ( $date[ 'hours' ] < 12 ) ? __( 'Ante Meridiem', 'iranium' ) : __( 'Post Meridiem', 'iranium' );
                    break;
                case 'B':
                    $output .= ( int )( 1 + ( $date[ 'mon' ] / 3 ) );
                    break;
                case 'g':
                    $output .= ( $date[ 'hours' ] > 12 ) ? $date[ 'hours' ] - 12 : $date[ 'hours' ];
                    break;
                case 'G':
                    $output .= $date[ 'hours' ];
                    break;
                case 'h':
                    $output .= sprintf( '%02d', ( $date[ 'hours' ] > 12 ) ? $date[ 'hours' ] - 12 : $date[ 'hours' ] );
                    break;
                case 'H':
                    $output .= sprintf( '%02d', $date[ 'hours' ] );
                    break;
                case 'i':
                    $output .= sprintf( '%02d', $date[ 'minutes' ] );
                    break;
                case 's':
                    $output .= sprintf( '%02d', $date[ 'seconds' ] );
                    break;
                case 'c':
                    $output = sprintf( '%d/%02d/%02d %02d:%02d:%02d', $date[ 'year' ], $date[ 'mon' ], $date[ 'mday' ], $date[ 'hours' ], $date[ 'minutes' ], $date[ 'seconds' ] );
                    break;
                case 'r':
                    $output = sprintf( '%s, %d %s %d, %02d:%02d:%02d', $this->week_days_names[ $date[ 'wday' ] + 1 ], $date[ 'mday' ], $this->month_names[ $date[ 'mon' ] - 1 ], $date[ 'year' ], $date[ 'hours' ], $date[ 'minutes' ], $date[ 'seconds' ] );
                    break;
                case 'U':
                    $output .= $timestamp;
                    break;
                default:
                    $output .= $format[ $i ];
            }
        }

        return $output;
    }

    /**
     * Returns the week day number.
     * 
     * @param int $wday
     * @return int
     */
    private function week_day( $wday ) {
        if ( $wday == 6 ) return 0;
        else ++$wday;
    }

    /**
     * Calculates Persian equivalent of given Gregorian date.
     * Please note that it does NOT do anythin with timezone.
     * 
     * @param int $gy Gregorian year
     * @param int $gm Gregorian month number
     * @param int $gd Gregorian day of month
     * @return array Array is like [ $py, $pm, $pd ]
     */
    public function gregorian_to_persian( $gy, $gm, $gd ) {
        if ( $gy > 1600 ) {
            $py = 979;
            $gy -= 1600;
        } else {
            $py = 0;
            $gy -= 621;
        }

        $gy2 = ( $gm > 2 ) ? ( $gy + 1 ) : $gy;
        $days = ( 365 * $gy ) + ( ( int )( ( $gy2 + 3 ) / 4 ) ) - ( ( int )( ( $gy2 + 99 ) / 100 ) ) + ( ( int )( ( $gy + 399 ) / 400 ) ) - 80 + $gd + $this->g_days_sum_month[ $gm - 1 ];
        $py += 33 * ( ( int )( $days / 12053 ) );
        $days %= 12053;
        $py += 4 * ( ( int )( $days / 1461 ) );
        $days %= 1461;
        if ( $days > 365 ) {
            $py += ( int )( ( $days - 1 ) / 365 );
            $days = ( $days - 1 ) % 365;
        }
        $pm = ( $days < 186 ) ? 1 + ( int )( $days / 31 ) : 7 + ( int )( ( $days - 186 ) / 30 );
        $pd = 1 + ( ( $days < 186 ) ? ( $days % 31 ) : ( ( $days - 186 ) % 30 ) );

        return [ $py, $pm, $pd ];
    }

    /**
     * Calculates Gregorian equivalent of the given Persian date.
     * 
     * @param int $py Persian year
     * @param int $pm Persian month number
     * @param int $pd Persian day of month
     * @return array Output is like [ $gy, $gm, $gd ]
     */
    public function persian_to_gregorian( $py, $pm, $pd ) {
        if ( $py > 979 ) {
            $gy = 1600;
            $py -= 979;
        } else {
            $gy = 621;
        }

        $days = ( 365 * $py ) + ( ( ( int )( $py / 33 ) ) * 8 ) + ( ( int )( ( ( $py % 33 ) + 3 ) / 4 ) ) + 78 + $pd + ( ( $pm < 7 ) ? ( $pm - 1 ) * 31 : ( ( $pm - 7 ) * 30 ) + 186 );
        $gy += 400 * ( ( int )( $days / 146097 ) );
        $days %= 146097;
        if ( $days > 36524 ) {
            $gy += 100 * ( ( int )( --$days / 36524 ) );
            $days %= 36524;
            if ( $days >= 365 ) $days++;
        }
        $gy += 4 * ( ( int )( $days / 1461 ) );
        $days %= 1461;
        if ( $days > 365 ) {
            $gy += ( int )( ( $days - 1 ) / 365 );
            $days = ( $days - 1 ) % 365;
        }
        $gd = $days + 1;
        foreach( [ 0, 31, ( ( $gy % 4 == 0 and $gy % 100 != 0 ) or ( $gy % 400 == 0 ) ) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ] as $gm => $v ) {
            if ( $gd <= $v ) break;
            $gd -= $v;
        }
        
        return [ $gy, $gm, $gd ];
    }

    /**
     * Replace English and Arabic digits with
     * standard Persian digits.
     * 
     * @since 1.0
     * @param string $input
     * @return string
     */
    public function persian_digits( $input ) {
        return str_replace( $this->arabic_digits, $this->persian_digits,
                str_replace( $this->english_digits, $this->persian_digits, $input ) );
    }

    /**
     * Replace Arabic Ke and Ye with proper
     * Persian characters.
     * 
     * @since 1.0
     * @param string $input
     * @return string
     */
    public function persian_characters( $input ) {
        return str_replace( $this->arabic_chars, $this->persian_chars, $input );
    }
}