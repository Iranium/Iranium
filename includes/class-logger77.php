<?php
/**
 * Iranium logger.
 * This class is created for debugging purposes, allowing
 * us to track down the plug-ins may conflict with Iridium.
 * 
 * @author ehsaan
 * @package Iranium
 * @subpackage Core/Debugging
 */

/**
 * No direct access allowed.
 */
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Iranium logger main class.
 */
final class Logger77 {
    /**
     * Log file path. Default is wp-content/iranium/debug.log
     */
    private $log_path;
    
    /**
     * Starts the logger.
     */
    public function __construct() {
        $this->log_path = apply_filters( 'iranium_log_path',
            IRANIUM_PLUGIN_DIR . '/debug.log'
        );
    }

    /**
     * Write a line to log file.
     * 
     * @param string $string
     * @param boolean $dates Include date or not.
     * @return true
     */
    private function _write( $string, $dates = true ) {
        if ( true === $dates ) {
            $line = sprintf( '[%s] %s' . PHP_EOL, date( 'r' ), $string );
        } else {
            $line = sprintf( '%s' . PHP_EOL, $string );
        }
        file_put_contents( $this->log_path, $line, FILE_APPEND | LOCK_EX );
        return true;
    }

    /**
     * Add an error line to log.
     * 
     * @param string $content
     * @return self
     */
    public function error( $content ) {
        $this->_write( '[ERR] ' . $content );
        return $this;
    }

    /**
     * Add a warning line to log.
     * 
     * @param string $content
     * @return self
     */
    public function warning( $content ) {
        $this->_write( '[WARN] ' . $content );
        return $this;
    }

    /**
     * Add a notice line to log.
     * 
     * @param string $content
     * @return self
     */
    public function notice( $content ) {
        $this->_write( '[NOTICE] ' . $content );
        return $this;
    }
}