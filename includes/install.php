<?php
/**
 * Installation hook checks if PHP version
 * and WordPress version are where we need
 * them to be.
 * 
 * @author ehsaan
 * @package Iranium
 * @subpackage Core/Installation
 */

register_activation_hook( IRANIUM_PLUGIN_FILE, 'iranium_installation' );

/**
 * Check the plug-in requirements.
 * For the current version, we check:
 * * PHP >= 5.6.0
 * * WordPress >= 4.8.0
 * 
 * @return void
 */
function iranium_installation() {
    global $wp_version;

    if ( version_compare( phpversion(), '5.6.0', '<' ) ) {
        deactivate_plugins( basename( IRANIUM_PLUGIN_FILE ) );
        return wp_die(
            sprintf(
                __( 'Sorry, the version of PHP that your server is using is older than Iranium can work correctly with. Iranium requires PHP version of %s or newer.', 'iranium' ),
                '5.6.0'
            ),
            __( 'Error' ),
            [
                'response'      => 200,
                'back_link'     => true
            ]
        );
    }

    if ( version_compare( $wp_version, '4.8.0', '<' ) ) {
        deactivate_plugins( basename( IRANIUM_PLUGIN_FILE ) );
        return wp_die(
            sprintf(
                __( 'Sorry, the version of WordPress that you are using is older than Iranium can work correctly with. Iranium requires WordPress version of 4.8.0 or newer.', 'iranium' ),
                '4.8.0'
            ),
            __( 'Error' ),
            [
                'response'      => 200,
                'back_link'     => true
            ]
        );
    }

    /**
     * I think we're good to go here.
     */
}