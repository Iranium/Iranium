<?php
/**
 * Exceptions for plug-ins may conflict with
 * Iridium.
 * We use debug_backtrace to detect which plugin
 * might conflict.
 * 
 * @author ehsaan
 * @package Iranium
 * @subpackage Compability
 */

add_filter( 'iranium_block_i18n_hook', 'iranium_check_plugins', 10, 3 );
/**
 * Check the conditions and return true
 * if date in Gregorian is needed.
 * 
 * @param $j Formatted date string
 * @param $format Format to display the date.
 * @param $timestamp Unix timestamp.
 * @return boolean True means we only need the date in Gregorian.
 */
function iranium_check_plugins( $j, $format, $timestamp ) {
    // Although, we don't need the parameters.

    $backtrace = debug_backtrace();
    
    /**
     * WooCommerce detection.
     * The problem is there when WooCommerce
     * requests the date in Y-m-d format. So
     * we stay alert on Y-m-d.
     */
    if ( 'Y-m-d' == $format && true === iranium_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        if ( WP_DEBUG )
            Iranium()->Logger->notice( 'Possible WooCommerce request.' )
                         ->notice( json_encode( [ $j, $format, $timestamp ] ) )
                         ->notice( var_export( $backtrace, true ) );

        /**
         * Numerous test cases show that the 7th
         * element of backtrace is where WooCommerce
         * gets engaged.
         */
        return ( ! empty( strstr( $backtrace[ 6 ][ 'file' ], 'woocommerce' ) ) );
    }

    /**
     * Standard W3C/ISO 8601 format replacement prevention.
     */
    if ( 'Y-m-d\TH:i:sP' == $format || 'c' == $format ) {
        return true;
    }

    /**
     * Post Expirator blocking. Because of variety of
     * the functions used in that plugin, we have to filter
     * all the requests coming from PE.
     */
    if ( true === iranium_plugin_active( 'post-expirator/post-expirator.php' ) ) {
        if ( true === iranium_check_trace( $backtrace, 'post-expirator' ) ) {
            if ( WP_DEBUG )
                Iranium()->Logger->notice( 'Request detected from post-expirator' );

            return true;
        }
    }

    /**
     * Slimstat blocking. Because of variety of
     * the functions used in that plugin, we have to filter
     * all the requests coming from SS.
     */
    if ( true === iranium_plugin_active( 'wp-slimstat/wp-slimstat.php' ) ) {
        if ( true === iranium_check_trace( $backtrace, 'wp-slimstat' ) ) {
            if ( WP_DEBUG )
                Iranium()->Logger->notice( 'Request detected from wp-slimstat' );

            return true;
        }
    }

    /**
     * Check for user-defined plug-ins.
     */
    if ( ! empty( iranium_get_blocked_plugins() ) ) {
        $blocked_plugins = iranium_get_blocked_plugins();
        
        foreach( $blocked_plugins as $plugin ) {
            if ( true === iranium_plugin_active( $plugin ) ) {

                if ( true === iranium_check_trace( $backtrace, explode( '/', $plugin )[ 0 ] ) ) {

                    if ( WP_DEBUG )
                        Iranium()->Logger->notice( 'Request detected from user-defined plugin : ' . $plugin );
                    return true;
                    
                }
            }
        }
    }

    return false; // Return false by default.
} 

/**
 * Checks if given plugin slug is active
 * or not.
 * 
 * @param string $slug Plugin slug.
 * @return boolean
 */
function iranium_plugin_active( $slug ) {
    if ( false === function_exists( ABSPATH . 'wp-admin/includes/plugin.php' ) )
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    
    return is_plugin_active( $slug );
}

/**
 * Checks the backtrace, returns true if
 * given string is there on the file names.
 * 
 * @param array $backtrace debug_backtrace() output
 * @param string $needle What we're looking for
 * @return boolean
 */
function iranium_check_trace( $backtrace, $needle ) { 
    foreach( $backtrace as $trace ) {
        if ( ! isset( $trace[ 'file' ] ) )
            continue;
        
        if ( ! empty( strstr( $trace[ 'file' ], $needle ) ) )
            return true;
        else
            continue;
    }
}

/**
 * Get blocked plug-ins specified in the
 * settings.
 * 
 * @return array
 */
function iranium_get_blocked_plugins() {
    global $iranium_options;

    $plugins = [];

    foreach ( $iranium_options as $k => $v ) {
        if ( substr( $k, 0, 16 ) == 'blocked_plugins-' && $v === '1' ) {
            $plugins[] = substr( $k, 16 );
        }
    }

    /**
     * Use blocked-plugins.php file for emergency cases
     * only.
     */
    require_once IRANIUM_PLUGIN_DIR . 'blocked-plugins.php';
    global $blocked_plugins;

    $plugins = array_merge( $plugins, $blocked_plugins );

    return $plugins;
}