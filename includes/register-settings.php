<?php
/**
 * Adds settings panel to the plugin.
 * Originally, wrote by Pippin Williamson
 * and the contributers from Easy Digtital Downloads.
 *
 * @author Pippin Williamson
 * @author ehsaan
 * @package Iranium
 * @subpackage Admin/Settings
 */
if ( ! defined( 'ABSPATH' ) ) exit; // No direct access.

add_action( 'admin_menu', 'iranium_add_settings_menu', 11 );

/**
 * Add Iranium settings page to the menu.
 * 
 * @see $iranium_options
 * @since 1.0
 * @return void
 */
function iranium_add_settings_menu() {
    add_menu_page(
        __( 'Iranium', 'iranium' ),
        __( 'Iranium', 'iranium' ),
        'manage_options',
        'iranium-settings',
        'iranium_render_settings',
        'dashicons-calendar-alt'
    );
}

/**
 * Retrieves settings from WP core
 *
 * @since           2.0
 * @return          array Iranium settings
 */
function iranium_get_settings() {
	$settings = get_option( 'iranium_options' );
	if ( empty( $settings ) ) {
		update_option( 'iranium_options', array(
            'force_locale'          =>  'disabled',
            'use_iridium'           =>  'enabled',
            'afghan_month_names'    =>  'disabled',
			'use_vazir_font'        =>  1,
			'blocked_plugins' 		=>	[]
		) );
	}

	return apply_filters( 'iranium_get_settings', $settings );
}

/**
 * Registers settings in WP core
 *
 * @since           2.0
 * @return          void
 */
function iranium_register_settings() {
	if ( false == get_option( 'iranium_options' ) ) {
		add_option( 'iranium_options' );
	}

	foreach ( iranium_get_registered_settings() as $tab => $settings ) {
		add_settings_section(
			'iranium_options_' . $tab,
			__return_null(),
			'__return_false',
			'iranium_options_' . $tab
		);

		foreach ( $settings as $option ) {
			$name = isset( $option['name'] ) ? $option['name'] : '';

			add_settings_field(
				'iranium_options[' . $option['id'] . ']',
				$name,
				isset( $option['type'] ) && function_exists( 'iranium_' . $option['type'] . '_callback' ) ? 'iranium_' . $option['type'] . '_callback' : 'iranium_missing_callback',
				'iranium_options_' . $tab,
				'iranium_options_' . $tab,
				array(
					'id'      => isset( $option['id'] ) ? $option['id'] : null,
					'desc'    => ! empty( $option['desc'] ) ? $option['desc'] : '',
					'name'    => isset( $option['name'] ) ? $option['name'] : null,
					'section' => $tab,
					'size'    => isset( $option['size'] ) ? $option['size'] : null,
					'options' => isset( $option['options'] ) ? $option['options'] : '',
					'std'     => isset( $option['std'] ) ? $option['std'] : ''
				)
			);
		}
	}
	register_setting( 'iranium_options', 'iranium_options', 'iranium_options_sanitize' );
}

add_action( 'admin_init', 'iranium_register_settings' );

/**
 * Gets settings tabs
 *
 * @since               2.0
 * @return              array Tabs list
 */
function iranium_get_tabs() {
	$tabs = array(
		'core'   		 	=> sprintf( __( '%s Core', 'iranium' ), '<span class="dashicons dashicons-admin-site"></span>' ),
		'plugins' 			=> sprintf( __( '%s Compability', 'iranium' ), '<span class="dashicons dashicons-admin-plugins"></span>' )
	);

	return $tabs;
}

/**
 * Sanitizes and saves settings after submit
 *
 * @since               2.0
 * @param               array $input Settings input
 * @return              array New settings to be saved
 */
function iranium_options_sanitize( $input = array() ) {

	global $iranium_options;

	if ( empty( $_POST['_wp_http_referer'] ) ) {
		return $input;
	}

	parse_str( $_POST['_wp_http_referer'], $referrer );

	$settings = iranium_get_registered_settings();
	$tab      = isset( $referrer['tab'] ) ? $referrer['tab'] : 'core';

	$input = $input ? $input : array();
	$input = apply_filters( 'iranium_options_' . $tab . '_sanitize', $input );

	// Loop through each setting being saved and pass it through a sanitization filter
	foreach ( $input as $key => $value ) {

		// Get the setting type (checkbox, select, etc)
		$type = isset( $settings[ $tab ][ $key ]['type'] ) ? $settings[ $tab ][ $key ]['type'] : false;

		if ( $type ) {
			// Field type specific filter
			$input[ $key ] = apply_filters( 'iranium_options_sanitize_' . $type, $value, $key );
		}

		// General filter
		$input[ $key ] = apply_filters( 'iranium_options_sanitize', $value, $key );
	}


	// Loop through the whitelist and unset any that are empty for the tab being saved
	if ( ! empty( $settings[ $tab ] ) ) {
		foreach ( $settings[ $tab ] as $key => $value ) {

			// settings used to have numeric keys, now they have keys that match the option ID. This ensures both methods work
			if ( is_numeric( $key ) ) {
				$key = $value['id'];
			}

			if ( $settings[$tab][ $key ][ 'type' ] == 'checkbox' && $settings[$tab][ $key ][ 'type' ] == 'multicheck' ) {
				if ( empty( $input[ $key ] ) ) {
					unset( $iranium_options[ $key ] );
				}
				if ( array_key_exists( $key, $input ) && $output[ $key ] === '-1' ) {
					unset( $iranium_options[ $key ] );
				}
			} else {
				if ( array_key_exists( $key, $input ) && empty( $input[ $key ] ) || ( array_key_exists( $key, $iranium_options ) && ! array_key_exists( $key, $input ) ) ) {
					unset( $iranium_options[ $key ] );
				}
			}
		}
	}

	// Merge our new settings with the existing
	$output = array_merge( $iranium_options, $input );
	global $iranium_options;
	$iranium_options = $output;

	add_settings_error( 'iranium-notices', '', __( 'Settings updated.', 'iranium' ), 'updated' );
	return $output;

}

/**
 * Get all installed plug-ins on the site.
 * 
 * @return array
 */
function iranium_get_plugins() {
	if ( ! function_exists( 'get_plugins' ) )
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	
	$list = [];
	$plugins = get_plugins();
	foreach( $plugins as $slug => $v ) {
		if ( str_replace( WP_PLUGIN_DIR . '/', '', IRANIUM_PLUGIN_FILE ) === $slug ) continue;
		$list[ $slug ] = esc_attr( $v[ 'Name' ] ) . ' - <code>' . esc_attr( $slug ) . '</code>';
	}
	return $list;
}

/**
 * Get settings fields
 *
 * @since           2.0
 * @return          array Fields
 */
function iranium_get_registered_settings() {
	$options  = array(
		'enabled'  => __( 'Enable', 'iranium' ),
		'disabled' => __( 'Disable', 'iranium' )
	);
	$settings = apply_filters( 'iranium_registered_settings', array(
		'core'    					=> apply_filters( 'iranium_core_settings', array(
			'force_locale'          =>  [
                'id'                =>  'force_locale',
                'name'              =>  __( 'Force WordPress locale', 'iranium' ),
                'type'              =>  'radio',
                'options'           =>  $options,
                'std'               =>  'disabled',
                'desc'              =>  __( 'Force WordPress locale to <code>fa_IR</code>.<br>You may need to update your WordPress installation.', 'iranium' )
            ],
            'use_iridium'           =>  [
                'id'                =>  'use_iridium',
                'name'              =>  __( 'Use Iranian calendar', 'iranium' ),
                'type'              =>  'radio',
                'options'           =>  $options,
                'std'               =>  'enabled',
                'desc'              =>  __( 'Use Iranian calendar system for all datestamps.', 'iranium' )
            ],
            'afghan_month_names'    =>  [
                'id'                =>  'afghan_month_names',
                'name'              =>  __( 'Use Afghanian month names', 'iranium' ),
                'type'              =>  'radio',
                'options'           =>  $options,
                'std'               =>  'disabled',
			],
            'use_vazir_font'        =>  [
                'id'                =>  'use_vazir_font',
                'name'              =>  sprintf( __( 'Use <a href="%s" target="_blank">Vazir font</a>', 'iranium' ), 'http://rastikerdar.github.io/vazir-font/' ),
                'type'              =>  'select',
                'options'           =>  [ 0 => __( 'Disabled', 'iranium' ), 1 => __( 'Only admin pages', 'iranium' ), 2 => __( 'Only Editor', 'iranium' ), 3 => __( 'Both', 'iranium' ) ],
				'std'               =>  1,
				'desc' 				=>	__( 'This option will work only if WordPress locale is set to Persian.', 'iranium' )
            ]
		) ),
		'plugins' 					=> apply_filters( 'iranium_plugins_compability_settings', array( 	
			'blocked_plugins'		=>	[
				'id' 				=>	'blocked_plugins',
				'name' 				=>	__( 'No Persian date for', 'iranium' ),
				'type' 				=>	'multicheck',
				'options' 			=>	iranium_get_plugins(),
				'desc' 				=>	__( 'Check the plug-ins you want not to use Persian calendar.', 'iranium' )
			]
		) )
	) );

	return $settings;
}

/* Form Callbacks Made by EDD Development Team */
function iranium_header_callback( $args ) {
	echo '<hr/>';
}

function iranium_checkbox_callback( $args ) {
	global $iranium_options;
	$option = isset( $iranium_options[ $args[ 'id' ] ] ) ? $iranium_options[ $args[ 'id' ] ] : [];
	$name = 'name=iranium_options[' . $args['id'] . ']';

	$checked  = ! empty( $option ) ? checked( 1, $option, false ) : '';
	$html     = '<input type="hidden"' . $name . ' value="-1" />';
	$html    .= '<input type="checkbox" id="iranium_options[' . esc_attr( $args['id'] ) . ']"' . $name . ' value="1" ' . $checked . '"/>';
	$html    .= '<label for="iranium_options[' . esc_attr( $args['id'] ) . ']"> '  . wp_kses_post( $args['desc'] ) . '</label>';

	echo $html;
}

function iranium_multicheck_callback( $args ) {
	global $iranium_options;

	$html = '';
	$html .= '<p class="description">' . $args['desc'] . '</p>';
	foreach ( $args['options'] as $key => $value ) {
		$option_name = $args['id'] . '-' . $key;
		
		echo '<p>';
		iranium_checkbox_callback( array(
			'id'   => $option_name,
			'desc' => $value
		) );
		echo '</p>';
	}

	echo $html;
}

function iranium_radio_callback( $args ) {
	global $iranium_options;

	foreach ( $args['options'] as $key => $option ) :
		$checked = false;

		if ( isset( $iranium_options[ $args['id'] ] ) && $iranium_options[ $args['id'] ] == $key ) {
			$checked = true;
		} elseif ( isset( $args['std'] ) && $args['std'] == $key && ! isset( $iranium_options[ $args['id'] ] ) ) {
			$checked = true;
		}

		echo '<input name="iranium_options[' . $args['id'] . ']"" id="iranium_options[' . $args['id'] . '][' . $key . ']" type="radio" value="' . $key . '" ' . checked( true, $checked, false ) . '/>';
		echo '<label for="iranium_options[' . $args['id'] . '][' . $key . ']">' . $option . '</label>&nbsp;&nbsp;';
	endforeach;

	echo '<p class="description">' . $args['desc'] . '</p>';
}

function iranium_text_callback( $args ) {
	global $iranium_options;

	if ( isset( $iranium_options[ $args['id'] ] ) ) {
		$value = $iranium_options[ $args['id'] ];
	} else {
		$value = isset( $args['std'] ) ? $args['std'] : '';
	}

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="text" class="' . $size . '-text" id="iranium_options[' . $args['id'] . ']" name="iranium_options[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<label for="iranium_options[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

	echo $html;
}

function iranium_number_callback( $args ) {
	global $iranium_options;

	if ( isset( $iranium_options[ $args['id'] ] ) ) {
		$value = $iranium_options[ $args['id'] ];
	} else {
		$value = isset( $args['std'] ) ? $args['std'] : '';
	}

	$max  = isset( $args['max'] ) ? $args['max'] : 999999;
	$min  = isset( $args['min'] ) ? $args['min'] : 0;
	$step = isset( $args['step'] ) ? $args['step'] : 1;

	$size = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
	$html = '<input type="number" step="' . esc_attr( $step ) . '" max="' . esc_attr( $max ) . '" min="' . esc_attr( $min ) . '" class="' . $size . '-text" id="iranium_options[' . $args['id'] . ']" name="iranium_options[' . $args['id'] . ']" value="' . esc_attr( stripslashes( $value ) ) . '"/>';
	$html .= '<label for="iranium_options[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

	echo $html;
}

function iranium_missing_callback( $args ) {
	echo '&ndash;';

	return false;
}

function iranium_select_callback( $args ) {
	global $iranium_options;

	if ( isset( $iranium_options[ $args['id'] ] ) ) {
		$value = $iranium_options[ $args['id'] ];
	} else {
		$value = isset( $args['std'] ) ? $args['std'] : '';
	}

	$html = '<select id="iranium_options[' . $args['id'] . ']" name="iranium_options[' . $args['id'] . ']"/>';

	foreach ( $args['options'] as $option => $name ) :
		$selected = selected( $option, $value, false );
		$html     .= '<option value="' . $option . '" ' . $selected . '>' . $name . '</option>';
	endforeach;

	$html .= '</select>';
	$html .= '<p class="description"><label for="iranium_options[' . $args['id'] . ']"> ' . $args['desc'] . '</label></p>';

	echo $html;
}

function iranium_render_settings() {
	global $iranium_options;
	$active_tab = isset( $_GET['tab'] ) && array_key_exists( $_GET['tab'], iranium_get_tabs() ) ? $_GET['tab'] : 'core';

	ob_start();
	?>
    <div class="wrap iranium-settings-wrap">
        <h1><?php _e( 'Iranium Settings', 'iranium' ) ?></h1>
        <h2 class="nav-tab-wrapper">
			<?php
			foreach ( iranium_get_tabs() as $tab_id => $tab_name ) {

				$tab_url = add_query_arg( array(
					'settings-updated' => false,
					'tab'              => $tab_id
				) );

				$active = $active_tab == $tab_id ? ' nav-tab-active' : '';

				echo '<a href="' . esc_url( $tab_url ) . '" title="' . strip_tags( $tab_name ) . '" class="nav-tab' . $active . '">';
				echo $tab_name;
				echo '</a>';
			}
			?>
        </h2>
		<?php settings_errors( 'iranium-notices' ); ?>
        <div id="tab_container">
            <form method="post" action="options.php">
				<?php
					if ( $active_tab == 'plugins' ) {
						echo sprintf( __( '<p>We are doing our best to keep Iranium compatible with popular plug-ins in the market. So far, we support a number of popular addons, including <strong>WooCommerce</strong>.</p>
<p>If you have found a plug-in that highly conflicts with Iranium, you can <a href="%s">report it to us</a> and we will look into it and if possible, provide a patch.</p>
<p>Meanwhile, if a plug-in keeps conflicting with Iranium, you can tell Iranium not to force it to use Iranian calendar.</p>', 'iranium' ), 'https://gitlab.com/Iranium/Iranium/issues' );
						
						echo iranium_notice_emergency_plugins();
					}
				?>
                <table class="form-table">
					<?php
					settings_fields( 'iranium_options' );
					do_settings_fields( 'iranium_options_' . $active_tab, 'iranium_options_' . $active_tab );
					?>
                </table>
				<?php submit_button(); ?>
            </form>
        </div><!-- #tab_container-->
    </div><!-- .wrap -->
	<?php
	echo ob_get_clean();
}

/**
 * Notice emergency plug-ins in blocked-plugins.php
 * file.
 * 
 * @since 1.0
 * @return void
 */
function iranium_notice_emergency_plugins() {
	require_once IRANIUM_PLUGIN_DIR . 'blocked-plugins.php';
	if ( ! empty( $blocked_plugins ) ) {
		echo '<p><strong>' . __( 'These plug-ins are listed in <code>blocked-plugins.php</code> file. You can\'t unblock them from this page.', 'iranium' ) . '</strong><br>';
		foreach( $blocked_plugins as $plugin ) {
			echo '<code>' . $plugin . '</code> &nbsp; ';
		}
	}
}