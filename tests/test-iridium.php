<?php
/**
 * Iridium tests
 * @package Iranium
 * @subpackage Tests
 */

/**
 * Iridium Tests class.
 */
class IridiumTest extends WP_UnitTestCase {
	/**
	 * Tests Gregorian to Persian functions
	 * including persian_date() function.
	 */
	function test_gregorian_to_persian() {
		$obj = Iranium()->Iridium;

		// Basic convert
		$this->assertEquals( $obj->persian_date( 'Y-m-d', '1999-12-18' ), '1378-09-27' );

		// Check if leap year works
		$this->assertEquals( $obj->persian_date( 'Y-m-d', '2021-3-20' ), '1399-12-30' );

		// Check the first day of month
		$this->assertEquals( $obj->persian_date( 'Y-m-d', '2021-2-19' ), '1399-12-01' );

		// Check the 31st day of Shahrivar
		$this->assertEquals( $obj->persian_date( 'Y-m-d', '2018-9-22' ), '1397-06-31' );

		// Check a full date (r)
		$this->assertEquals( $obj->persian_date( 'r', '2016-8-14' ), 'Sunday, 24 Mordad 1395, 00:00:00' );

		// Let us travel a little more far.
		$this->assertEquals( $obj->persian_date( 'r', '2051-8-22' ), 'Tuesday, 31 Mordad 1430, 00:00:00' );
	}

	/**
	 * Tests Persian to Gregorian functions.
	 */
	function test_persian_to_gregorian() {
		$obj = Iranium()->Iridium;
		
		// Basic convert
		$this->assertEquals( $obj->persian_to_gregorian( 1378, 9, 27 ), [ 1999, 12, 18 ] );

		// Check if leap year works
		$this->assertEquals( $obj->persian_to_gregorian( 1399, 12, 30 ), [ 2021, 3, 20 ] );

		// Let us travel a little more far.
		$this->assertEquals( $obj->persian_to_gregorian( 1430, 5, 31 ), [ 2051, 8, 22 ] );
	}

	/**
	 * Tests characters replace functions.
	 */
	function test_characters() {
		$obj = Iranium()->Iridium;

		// Only letters
		$this->assertEquals( $obj->persian_characters( 'كي ة' ), 'کی ه' );

		// Only English digits
		$this->assertEquals( $obj->persian_digits( '0123456789' ), '۰۱۲۳۴۵۶۷۸۹' );

		// Only Arabic digits
		$this->assertEquals( $obj->persian_digits( '٦٥٤' ), '۶۵۴' );
	}
}
